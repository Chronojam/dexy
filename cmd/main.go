// Copyright © 2017 Calliope Gardner <calliope@chronojam.co.uk>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"encoding/json"

	"gitlab.com/chronojam/dexy/pkg/config"
	"gitlab.com/chronojam/dexy/pkg/oidc"
)

var (
	dexyConfigPath = flag.String("config.path", os.Getenv("DEXY_CONFIG"), "config path for dexy.")
	clientId       = flag.String("client.id", os.Getenv("DEXY_CLIENT_ID"), "client id for dexy.")
	clientSecret   = flag.String("client.secret", os.Getenv("DEXY_CLIENT_SECRET"), "client secret for dexy.")
	callbackHost   = flag.String("callback.host", os.Getenv("DEXY_CALLBACK_HOST"), "callback host for dexy.")
	callbackPort   = flag.Int("callback.port", PortEnvvarOrDie(os.Getenv("DEXY_CALLBACK_PORT")), "callback port for dexy.")
	scopes         = flag.String("scopes", os.Getenv("DEXY_SCOPES"), "comma seperated list of scopes, eg; email,group")
	host           = flag.String("host", os.Getenv("DEXY_HOST"), "host for dexy.")
	output         = flag.String("output", os.Getenv("DEXY_OUTPUT"), "output for dexy k8s|json")
)

var Version = "dev"

func PortEnvvarOrDie(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		return 10111
	}
	return i
}

type ExecCredential struct {
	ApiVersion string `json:"apiVersion"`
	Kind       string `json:"kind"`

	Status ExecCredentialStatus `json:"status"`
}
type ExecCredentialStatus struct {
	Token               string    `json:"token"`
	ExpirationTimestamp time.Time `json:"expirationTimestamp"`
}

func main() {
	flag.Parse()

	command := flag.Arg(0)

	switch command {
	case "version":
		fmt.Printf("dexy - a simple 3lo cli tool, version %s\n", Version)
		return
	}

	homedir := os.Getenv("HOME")
	// No defaults by default.
	c := config.Empty(). // no defaults
				Read("/etc/dexy.yaml").                      // system dexy
				Read(fmt.Sprintf("%v/.dexy.yaml", homedir)). // home dexy
				Read(*dexyConfigPath).
				With(
			config.New(*clientId, *clientSecret, *callbackHost, *callbackPort, *host, strings.Split(*scopes, ","), *output),
		) // envvars and command line

	tok := oidc.GetToken(c)

	var b []byte
	var err error
	switch *c.OutputFormat {
	case "k8s":
		e := ExecCredential{
			ApiVersion: "client.authentication.k8s.io/v1",
			Kind:       "ExecCredential",
			Status: ExecCredentialStatus{
				Token:               tok.AccessToken,
				ExpirationTimestamp: tok.ExpiryTime,
			},
		}
		b, err = json.Marshal(e)
		if err != nil {
			log.Fatalf("error while marshalling token from provider: %v", err)
		}
	default:
		b, err = json.Marshal(tok)
		if err != nil {
			log.Fatalf("error while marshalling token from provider: %v", err)
		}
	}

	fmt.Println(string(b))
}
