// Copyright © 2017 Calliope Gardner <calliope@chronojam.co.uk>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package oidc

import (
	"io/ioutil"
	"time"
	"fmt"
	"log"
	"context"
	"net/http"
	"encoding/json"

	"gitlab.com/chronojam/dexy/pkg/config"

	"github.com/pkg/browser"
	"github.com/coreos/go-oidc"
	"golang.org/x/oauth2"
)

type TokenData struct {
	AccessToken string    `json:"access_token"`
	ExpiryTime  time.Time `json:"expiry_time"`
}


// GetToken retrieves a non-expired token from disk,
// or fetches a new token using the passed in Config
func GetToken(config *config.Config) TokenData {
	b, err := ioutil.ReadFile(config.TokenFile())
	if err == nil {
		var tok TokenData
		err = json.Unmarshal(b, &tok)
		if err != nil {
			log.Fatalf("error while unmarshalling token from file: %v", err)
		}
		if tok.ExpiryTime.After(time.Now()) {
			return tok
		}
	}

	// Token either expired, or doesnt exist.
	provider, err := oidc.NewProvider(context.Background(), *config.Host)
	if err != nil {
		log.Fatalf("error while creating new oidc provider: %v", err)
	}
	scopes := []string{oidc.ScopeOpenID}
	scopes = append(scopes, config.Scopes...)
	oauth2Config := oauth2.Config{
		ClientID:     *config.ClientID,
		ClientSecret: *config.ClientSecret,
		RedirectURL:  config.CallbackURL(),
		Endpoint:     provider.Endpoint(),
		Scopes:       scopes,
	}

	tokenChan := make(chan TokenData)
	w := &CallbackHandler{
		verifier: provider.Verifier(&oidc.Config{ClientID: *config.ClientID}),
		cfg: oauth2Config,
		tokenChan: tokenChan,
	}
	go w.Serve()

	err = browser.OpenURL(oauth2Config.AuthCodeURL(""))
	if err != nil {
		log.Fatalf("error while opening new browser window: %v", err)
	}
	tok := <-tokenChan

	b, err = json.Marshal(tok)
	if err != nil {
		log.Fatalf("error while marshalling token from provider: %v", err)
	}
	
	err = ioutil.WriteFile(config.TokenFile(), b, 0755)
	if err != nil {
		log.Fatalf("error while attempting to write token to file: %v", err)
	}
	return tok
}


type CallbackHandler struct {
	verifier *oidc.IDTokenVerifier
	cfg oauth2.Config
	tokenChan chan TokenData
}


func (s *CallbackHandler) Serve() {
	http.HandleFunc("/oauth2/callback", s.oauth2Callback);
	http.ListenAndServe(":10111", nil)
}

func (s *CallbackHandler) oauth2Callback(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	oauth2Token, err := s.cfg.Exchange(ctx, r.URL.Query().Get("code"))
	if err != nil {
		log.Fatalf("error while attempting initial token exchange %v", err)
	}

	// Extract the ID Token from OAuth2 token.
	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		// handle missing token
		log.Fatalf("missing id_token")
	}

	// Parse and verify ID Token payload.
	idToken, err := s.verifier.Verify(ctx, rawIDToken)
	if err != nil {
		fmt.Println(err.Error())
		// handle error
	}

	ret := TokenData{
		AccessToken: rawIDToken,
		ExpiryTime:  idToken.Expiry,
	}
	s.tokenChan <- ret

	fmt.Fprintf(w, "Done, you can now close this window")
}