// Copyright © 2017 Calliope Gardner <calliope@chronojam.co.uk>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
package config

import (
	"os"
	"fmt"
	"io/ioutil"
	"errors"
	"strings"

	"crypto/md5"
	"gopkg.in/yaml.v2"
)

type Config struct {
	ClientID *string `yaml:"client_id"`
	ClientSecret *string `yaml:"client_secret"`
	CallbackHost *string `yaml:"callback_host"`
	CallbackPort int `yaml:"callback_port"`
	Host *string `yaml:"host"`
	Scopes []string `yaml:"scopes"`
	OutputFormat *string `yaml:"outputFormat"`
}

func NilOnEmpty(value string) *string {
	if value == "" {
		return nil
	}
	return &value
}

func New(
	ClientID string,
	ClientSecret string,
	CallbackHost string,
	CallbackPort int,
	Host string,
	Scopes []string,
	OutputFormat string,
) *Config {
	return &Config{
		ClientID: NilOnEmpty(ClientID),
		ClientSecret: NilOnEmpty(ClientSecret),
		CallbackHost: NilOnEmpty(CallbackHost),
		CallbackPort: CallbackPort,
		Host: NilOnEmpty(Host),
		Scopes: Scopes,
		OutputFormat: NilOnEmpty(OutputFormat),
	}
}

func Empty() *Config {
	return &Config{
		ClientID: NilOnEmpty(""),
		ClientSecret: NilOnEmpty(""),
		CallbackHost: NilOnEmpty(""),
		CallbackPort: 10111,
		Host: NilOnEmpty(""),
		Scopes: []string{},
		OutputFormat: NilOnEmpty(""),
	}
}

// Reads a config from a location
// panic for errors.
func (c *Config) Read(path string) *Config {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return c.With(&Config{})
		}
		panic(err)
	}

	newConfig := &Config{}
	err = yaml.Unmarshal(b, newConfig)
	if err != nil {
		panic(err)
	}
	
	return c.With(newConfig)
}

// Merge 2 configs together, the second configuration
// values take precendence
func (c *Config) With(config *Config) *Config {
	ret := c
	if config.ClientID != nil {
		ret.ClientID = config.ClientID
	}
	if config.ClientSecret != nil {
		ret.ClientSecret = config.ClientSecret
	}
	if config.CallbackHost != nil {
		ret.CallbackHost = config.CallbackHost
	}
	if config.CallbackPort != 0 {
		ret.CallbackPort = config.CallbackPort
	}
	if config.Host != nil {
		ret.Host = config.Host
	}
	if config.OutputFormat != nil {
		ret.OutputFormat = config.OutputFormat
	}

	// Merge all the scopes together.
	mapScope := map[string]string{}
	for _, x := range append(c.Scopes, config.Scopes...) {
		mapScope[x] = ""
	}
	scopes := []string{}
	for x, _ := range mapScope {
		scopes = append(scopes, x)
	}
	ret.Scopes = scopes

	return ret
}

func (c *Config) Hash() string {
	hash := "" + 
		*c.ClientID + 
		*c.ClientSecret + 
		*c.CallbackHost + 
		*c.Host + 
		fmt.Sprintf("%d", c.CallbackPort) +
		strings.Join(c.Scopes, "") + 
		*c.OutputFormat

	return fmt.Sprintf("%x", md5.Sum([]byte(hash)))
}

func (c *Config) TokenFile() string {
	homedir := os.Getenv("HOME")
	if homedir == "" {
		// panic for now.
		panic("HOME not set, unable to find token file :(")
	}

	return fmt.Sprintf("%v/.dexy-%s.json", homedir, c.Hash())
}

func (c *Config) CallbackURL() string {
	return fmt.Sprintf("http://%s:%d/oauth2/callback", *c.CallbackHost, c.CallbackPort)
}