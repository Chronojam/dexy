{
    description = "Development Environment";

    inputs = {
        nixpkgs.url = github:NixOS/nixpkgs/21.11;
        flake-utils.url = "github:numtide/flake-utils";
    };

    outputs = { self, nixpkgs, flake-utils }:
        flake-utils.lib.eachDefaultSystem (system: 
            let pkgs = nixpkgs.legacyPackages.${system};
            
            in {
                defaultPackage = (import ./default.nix { 
                    buildBazelPackage = pkgs.buildBazelPackage.override { bazel = pkgs.bazel_4; };
                    git = pkgs.git;
                    lib = pkgs.lib;
                    cacert = pkgs.cacert;
                    go = pkgs.go;
                });
                devShell = 
                    (pkgs.buildFHSUserEnv {
                        name = "dexy";
                        targetPkgs = pkgs: [
                            pkgs.bazel_4
                            pkgs.stdenv
                            pkgs.gcc

                            pkgs.go
                            pkgs.openjdk
                            #pkgs.glibc.static
                        ];
                    }).env;
            });
}