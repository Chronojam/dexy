**Dexy**
----------
**Introduction**    

Dexy is a simple command line tool to do 3LO authentication with an external provider and give a token back.

**Installation**

You can find releases for linux and mac on the releases page.

**Usage**    

Dexy works on client_id/client_secret  pairs.
As such you'll need to generate one and authorize it with your provider. In [dex](https://github.com/coreos/dex) this might look like this:

```
staticClients:
- id: dexy
  secret: dexy-secret
  name: 'Dexy'
  redirectURIs:
  # This needs to be a local address, as dexy will start a webserver
  # for you to callback too, it will need to match the host/port in the dexy config
  - 'http://localhost:10111/oauth2/callback'`
```
Dexy also has its own configuration file, which it will search for in the following locations:
```
- /etc/dexy.yaml
- $HOME/.dexy.yaml
- $DEXY_CONFIG
``` 
```
host: "https://dex.mycompany.com"
callback_host: "localhost"
callback_port: 10111
client_id: "dexy"
client_secret: "dexy-secret"
scopes
  - [ Optional list of scopes ]
```

It can only support a single provider at a time, if you need to change providers, you can delete the dexy token file in ~/.dexy-token.yaml

**Building**    

Dexy uses bazel to build
```
bazel build //cmd

# To run tests
bazel test //...
```

**Contributing**    

All PR's are welcome, just open one aganist main

**NixOS**
If you are cool like me, there is also support a nix derivation for dexy:

Development Environment:
```
nix develop
```
Build binary for your system:
```
nix build
```
