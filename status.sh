#!/usr/bin/env bash

# get tag and number of commits since
# can use --abbrev=0 to just get the tag name if desired
GIT_TAG=$(git describe --tags)
if [ -z "$GIT_TAG" ]; then
  GIT_TAG=$(git rev-parse --short HEAD)
fi


echo STABLE_GIT_COMMIT $GIT_TAG