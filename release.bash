#/usr/bin/env bash

for platform in "linux_amd64" "darwin_amd64"; do
    bazel build --output_base output --@io_bazel_rules_go//go/config:static --platforms=@io_bazel_rules_go//go/toolchain:$platform //cmd
    curl --header "PRIVATE-TOKEN: $CI_JOB_TOKEN" \
     --upload-file bazel-bin/cmd/cmd_/cmd \
     "https://gitlab.com/api/v4/projects/Chronojam%2Fdexy/packages/generic/binaries/3.0.0/$platform?status=default"
done
